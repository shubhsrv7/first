-- user registration table 
create table registration(
Fname varchar(20) NOT NULL,
Lname varchar(20),
Gender char(1),
DOB date,
Utype char(1),
Address varchar(50),
Phone bigint,
created_on timestamp,
modified_on timestamp,
created_by varchar(30),
modified_by varchar(30),
status int
);

-- book details table
create table bookdetails(
Title varchar(30),
Author varchar(30),
Publisher varchar(30),
Genre varchar(10),
YearOfRelease year,
Rating int,
created_on timestamp,
modified_on timestamp,
created_by varchar(30),
modified_by varchar(30),
status int
);

-- author details table
create table authordetails(
AuthorName varchar(30),
DOB date,
Description varchar(50),
created_on timestamp,
modified_on timestamp,
created_by varchar(30),
modified_by varchar(30),
status int
);

-- publisher details table
create table publisherdetails(
PublisherName varchar(30),
EstdOn year,
Address varchar(30),
Description varchar(50),
created_on timestamp,
modified_on timestamp,
created_by varchar(30),
modified_by varchar(30),
status int
);

-- genre table
create table genre(
genre varchar(10)
);